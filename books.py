from typing import Optional
from fastapi import FastAPI
from fastapi.params import Body

app = FastAPI()

BOOKS = {
    'book_1': {'title': 'Title One', 'author': 'Author One'},
    'book_2': {'title': 'Title Two', 'author': 'Author Two'},
    'book_3': {'title': 'Title Three', 'author': 'Author Three'},
    'book_4': {'title': 'Title Four', 'author': 'Author Four'},
    'book_5': {'title': 'Title Five', 'author': 'Author Five'},
    'book_6': {'title': 'Title Six', 'author': 'Author Six'},

}

@app.get('/')
async def read_all(skip_book: Optional[str]= None):
    if skip_book:
        new_books = BOOKS.copy()
        del new_books[skip_book]
        return new_books
    
    return BOOKS


@app.get('/book/{book_name}')
async def read_book(book_name):
    try:
        book = BOOKS[book_name]
    except:
        book = {'Error': 'Not found'}
    return book


@app.post('/')
async def create_book(title, author):
    current_book_id = 0

    if len(BOOKS) > 0:
        for book in BOOKS:
            x = int(book.split('_')[-1])
            if x > current_book_id:
                current_book_id = x
            
    BOOKS[f'book_{current_book_id + 1}'] = {'title': title, 'author': author}
    return BOOKS[f'book_{current_book_id + 1}']


@app.put('/{name_id}')
async def update_book(title: str, author: str, name_id: str):
    new_book = {'title': title, 'author': author}
    BOOKS[name_id] = new_book
    return new_book


@app.delete('/{name_id}')
async def delete_book(name_id: str):
    del BOOKS[name_id]
    return f'{name_id} deleted.'

