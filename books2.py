from fastapi import FastAPI, HTTPException, Request, status, Form
from fastapi.param_functions import Header
from pydantic import BaseModel, Field
from typing import Optional
from uuid import UUID
from starlette.responses import JSONResponse


class NegativeNumberException(Exception):
    def __init__(self, books_to_return):
        self.books_to_return = books_to_return


app = FastAPI()


class Book(BaseModel):
    id: UUID
    title: str = Field(min_length=1)
    author: str = Field(min_length=1, max_length=100)
    description: str = Field(
        title='Description of the book', max_length=100, min_length=1)
    rating: int = Field(gt=-1, lt=101)

    class Config:
        schema_extra = {
            'example': {
                'id': '0cbd4b9e-9805-4718-82c9-4c3d62dd82f4',
                'title': 'Computer',
                'author': 'Code',
                'description': 'Teste',
                'rating': 10
            }
        }


class BookNoRating(BaseModel):
    id: UUID
    title: str = Field(min_length=1)
    author: str
    description: str = Field(
        title='Description of the book', max_length=100, min_length=1)


BOOKS = []

@app.get('/header')
async def read_header(random_header: Optional[str] = Header(None)):
    return {'Random-Header': random_header}



@app.exception_handler(NegativeNumberException)
async def negative_number_exception_handler(request: Request, exception: NegativeNumberException):
    return JSONResponse(
        status_code=418,
        content={'message': f'Hey, why do you want {exception.books_to_return}'}
    )


@app.post('/books/login/')
async def book_login(username: str = Form(...), password: str = Form(...)):
    return {'username': username, 'password': password}

@app.put('/book/{book_id}', status_code=status.HTTP_204_NO_CONTENT)
async def update_book(book_id: UUID, book: Book):
    counter = 0
    for i in BOOKS:
        counter += 1
        if i.id == book_id:
            BOOKS[counter - 1] = book
            return BOOKS[counter - 1]
    raise raise_item_cannot_be_found_exception()


@app.delete('/book/{book_id}')
async def delete_book(book_id: UUID):
    counter = 0
    for i in BOOKS:
        counter += 1
        if i.id == book_id:
            del BOOKS[counter - 1]
            return {'success': 'Books deleted'}
    raise raise_item_cannot_be_found_exception()


@app.get('/book/rating/{book_id}', response_model=BookNoRating)
async def read_book_no_rating(book_id: UUID):
    for x in BOOKS:
        if x.id == book_id:
            return x
    raise raise_item_cannot_be_found_exception()

@app.get('/book/{book_id}')
async def read_book(book_id: UUID):
    for x in BOOKS:
        if x.id == book_id:
            return x


@app.get('/')
async def read_all(book_to_return: Optional[int] = None):
    if book_to_return and book_to_return < 0:
        raise NegativeNumberException(books_to_return=book_to_return)

    if len(BOOKS) < 1:
        create_books_apis()
    if book_to_return and len(BOOKS) >= book_to_return > 0:
        i = 1
        new_books = []
        while i <= book_to_return:
            new_books.append(BOOKS[i-1])
            i += 1
        return new_books
    return BOOKS


@app.post('/', status_code=status.HTTP_201_CREATED)
async def create_book(book: Book):
    BOOKS.append(book)
    return book


def create_books_apis():
    book_1 = Book(
        id='0cbd4b9e-9805-4718-82c9-4c3d62dd82f4',
        title='The Book',
        author='Pedro Fonseca',
        description='The books is amazing',
        rating=100)

    book_2 = Book(
        id='0cbd4b9e-9805-4718-82c9-4c3d62dd82f4',
        title='The Book',
        author='Pedro Fonseca',
        description='The books is amazing',
        rating=100)

    book_3 = Book(
        id='0cbd4b9e-9805-4718-82c9-4c3d62dd82f4',
        title='The Book',
        author='Pedro Fonseca',
        description='The books is amazing',
        rating=100)

    book_4 = Book(
        id='0cbd4b9e-9805-4718-82c9-4c3d62dd82f4',
        title='The Book',
        author='Pedro Fonseca',
        description='The books is amazing',
        rating=100)

    BOOKS.append(book_1)
    BOOKS.append(book_2)
    BOOKS.append(book_3)
    BOOKS.append(book_4)


def raise_item_cannot_be_found_exception():
    return HTTPException(status_code=404, detail='Book not found')
