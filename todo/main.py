from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from models import Base
from database import engine
from routers import auth, todos, file


app = FastAPI()

Base.metadata.create_all(bind=engine)


app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

app.mount('/files', StaticFiles(directory='files'), name='files')

app.include_router(auth.router)
app.include_router(todos.router)
app.include_router(file.router)
