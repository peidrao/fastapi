from settings import client


def test_get_all_todos():
    response = client.get('/todos/')
    assert response.status_code == 200
