from settings import client


def test_auth_error():
    response = client.post('/auth/users/authenticate/',
                           data={'username': '', 'password': ''})
    access_token = response.json().get('access_token')
    assert access_token == None


def test_auth_success():
    response = client.post('auth/users/authenticate/',
                           data={'username': 'marcos', 'password': 'tecarch123'})

    access_token = response.json().get('token')
    assert access_token


def test_create_user():
    response = client.post('auth/users/create/',
                           json={
                               'username': 'msarcos',
                               'email': 'marscos@gmail.com',
                               'password': 'tecarch123',
                               'first_name': 'Jose',
                               'last_name': 'Luis'
                           })

    assert response.status_code == 201
