from typing import List
from fastapi import Depends, APIRouter, status, Response
from fastapi.encoders import jsonable_encoder
from starlette.responses import JSONResponse

from models import Base, Todo as TodoDB
from database import engine, get_db
from sqlalchemy.orm import Session

from .auth import get_current_user
from exceptions import UserException, TodoException
from schemas import TodoDisplay, TodoCreate


router = APIRouter(
    prefix='/todos',
    tags=['todos'],
    responses={401: {'user': 'Todo not found'}}
)

Base.metadata.create_all(bind=engine)


@router.get('/', summary='Get All todos', description='All todos', response_model=List[TodoDisplay])
async def get_all(db: Session = Depends(get_db)):
    return db.query(TodoDB).all()


@router.get('/todos/users/')
async def read_all_by_user(user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    if not user:
        raise UserException.get_user_exception()
    else:
        return db.query(TodoDB).filter(TodoDB.owner_id == user.get('id')).all()


@router.get('/todo/{id}')
async def get_by_id(user: dict = Depends(get_current_user),  db: Session = Depends(get_db), id: int = None):
    todo = db.query(TodoDB).filter(TodoDB.id == id).filter(
        TodoDB.owner_id == user.get('id')).first()

    if not user:
        raise UserException.get_user_exception()
    if todo:
        return todo
    raise TodoException.get_todo_excetion()


@router.post('/todo/')
async def create_todo(todo: TodoCreate, user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    todo_model = TodoDB(
        title=todo.title,
        description=todo.description,
        priority=todo.priority,
        complete=todo.complete,
        owner_id=user.get('id')
    )
    db.add(todo_model)
    db.commit()

    return JSONResponse(content=jsonable_encoder(todo), status_code=status.HTTP_201_CREATED)


@router.put('/todo/{id}')
async def update_todo(id: int, todo: TodoCreate, user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    todo_model = db.query(TodoDB).filter(TodoDB.id == id).filter(
        TodoDB.owner_id == user.get('id')).first()

    if not user:
        raise UserException.get_user_exception()

    if not todo_model:
        raise TodoException.get_todo_excetion()

    todo_model.title = todo.title
    todo_model.description = todo.description
    todo_model.priority = todo.priority
    todo_model.complete = todo.complete

    db.add(todo_model)
    db.commit()

    return JSONResponse(content=jsonable_encoder(todo), status_code=status.HTTP_200_OK)


@router.delete('/todo/{id}')
async def destroy_todo(id: int, user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    todo_model = db.query(TodoDB).filter(TodoDB.id == id).filter(
        TodoDB.owner_id == user.get('id')).first()

    if not user:
        raise UserException.get_user_exception()

    if not todo_model:
        raise TodoException.get_todo_excetion()


    db.delete(todo_model)
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)
