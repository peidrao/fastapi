import shutil

from fastapi import APIRouter, File, UploadFile
from fastapi.responses import FileResponse


router = APIRouter(prefix='/file', tags=['file'])


@router.post('/file')
async def get_file(file: bytes = File(...)):
    content = file.decode('utf-8')
    lines = content.split('\n')
    return {'lines': lines}


@router.post('/upload')
async def get_upload_file(image: UploadFile = File(...)):
    path = f'files/{image.filename}'
    with open(path, 'wb') as buffer:
        shutil.copyfileobj(image.file, buffer)

    return {
        'filename': image.filename,
        'type': image.content_type
    }


@router.get('/download/{filename}', response_class=FileResponse)
async def download_file(filename: str):
    path = f'files/{filename}'
    return path
