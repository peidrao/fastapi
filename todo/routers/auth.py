from datetime import datetime, timedelta
from jose import jwt, JWTError
from typing import List, Optional
from sqlalchemy.orm.session import Session

from fastapi import Depends, APIRouter, status
from fastapi.responses import JSONResponse
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.encoders import jsonable_encoder

from database import get_db
from schemas import UserCreate, UserDisplay
from models import User
from exceptions import UserException
from utils import oauth2_bearer, password_hash, verify_password


SECRET_KEY = 'Kad54das123kj1asd1xavvsd615213llhbnzy'
ALGORITHM = 'HS256'
ACCESS_TOKEN_EXPIRE_MINUTES = 30


router = APIRouter(
    prefix='/auth',
    tags=['auth'],
    responses={401: {'user': 'User not authorized'}}
)


def authenticate_user(username: str, password: str, db):
    user = db.query(User).filter(User.username == username).first()

    if not user:
        raise UserException.get_user_exception()

    if not verify_password(password, user.password):
        raise UserException.get_user_credentials_excetion()
    return user


def create_access_token(username: str, user_id: int, expires_delta: Optional[timedelta] = None):
    encode = {'sub': username, 'id': user_id}
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)

    encode.update({'exp': expire})
    return jwt.encode(encode, SECRET_KEY, algorithm=ALGORITHM)


def get_current_user(token: str = Depends(oauth2_bearer)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get('sub')
        user_id: str = payload.get('id')

        if username is None or user_id is None:
            raise UserException.get_user_credentials_excetion()
        return {'username': username, 'id': user_id, 'token': token}

    except JWTError:
        raise UserException.get_user_credentials_excetion()


@router.get('/users/', response_model=List[UserDisplay])
async def read_all(db: Session = Depends(get_db)):
    return db.query(User).all()


@router.post('/users/authenticate/')
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = authenticate_user(form_data.username, form_data.password, db)
    if not user:
        raise UserException.token_exception()
    token_expires = timedelta(minutes=20)
    token = create_access_token(
        user.username, user.id, expires_delta=token_expires)

    return get_current_user(token)


@router.post('/users/create/')
async def create_user(user: UserCreate, db: Session = Depends(get_db)):
    user_exists = db.query(User).filter(User.username == user.username).first()
    
    if user_exists:
        raise UserException.get_user_exists_exception()

    user_model = User()
    user_model.email = user.email
    user_model.username = user.username
    user_model.first_name = user.first_name
    user_model.last_name = user.last_name
    user_model.password = password_hash(user.password)

    db.add(user_model)
    db.commit()
    return JSONResponse(content=jsonable_encoder(user), status_code=status.HTTP_201_CREATED)
