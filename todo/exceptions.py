from fastapi import HTTPException, status


class UserException(Exception):

    def get_user_credentials_excetion():
        credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Could not validate credentials', headers={'WWW-Authenticate': 'Bearer'})
        return credentials_exception

    def token_exception():
        token_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password', headers={'WWW-Authenticate': 'Bearer'})
        return token_exception

    def get_user_exception():
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='User not found')

    def get_user_exists_exception():
        return HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='User already exists')


class TodoException(Exception):

    def get_todo_excetion():
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Todo not found')
