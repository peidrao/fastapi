from typing import Optional
from pydantic import BaseModel, Field


class UserCreate(BaseModel):
    username: str
    email: Optional[str]
    first_name: str
    last_name: str
    password: str


class UserDisplay(BaseModel):
    username: str
    email: Optional[str]
    first_name: str
    last_name: str

    class Config:
        orm_mode = True


class TodoCreate(BaseModel):
    title: str
    description: Optional[str]
    priority: int = Field(
        gt=0, lt=6, description='The priority must be between 1-5')
    complete: bool


class TodoDisplay(BaseModel):
    title: str
    description: str
    priority: int
    complete: bool
    owner: UserDisplay

    class Config:
        orm_mode = True
